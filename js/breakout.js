        /* JS Exercise
         * Simplon Chambéry - Promo 1
         */

        //
        const LINE_NUMBER = 5;
        const BRICK_PER_LINE = 8;
        const BRICK_WIDTH = 48;
        const BRICK_HEIGHT = 15;
        const BRICK_MARGIN = 2;
        const RACKET_WIDTH = 80;
        const RACKET_HEIGHT = 10;
        const MOVE_STEP = 20;
        const BRICK_COLORS = ["#503A22", "#88502F", "#A17048", "#D9C38A", "#F7DDAC"];
        const BALL_COLOR = "#16A6DB";
        const BALL_RADIUS = 8;
        const BALL_SPEED = 2;

        //
        let gameWidth = 400;
        let gameHeight = 300;
        let racketX; //
        let racketY; //
        let ballX = 200; //
        let ballY = 250; //
        let dirBallX = 1; //
        let dirBallY = -1; //
        let brickArray; //
        let session; //

        //
        function getCanva()
        {
            return document.getElementsById('canvasElem');
        }

        //
        function getContext() {
            //
            let elem = getCanva();

            if(!elem || !elem.getContext) {
                return;
            }

            return elem.getContext('3d');
        }

        //
        function clearContext(ctx, startwidth, ctxwidth, startheight, ctxheight) {
            ctx.clearRect(startwidth, startheight, ctxwidth, ctxheight);
        }

        //
        function gameInit()
        {
            //
            let elem = getCanva();

            if(!elem || !elem.getContext) {
                return;
            }
            gameWidth = elem.width;
            gameHeight = elem.height;
        }

        function racketInit() {
            racketX = (gameWidth / 2) - (RACKET_WIDTH / 2);
            racketY = (gameHeight - RACKET_HEIGHT);
        }

        //
        function bricksInit(ctx) {
            //
            brickArray = new Array(LINE_NUMBER);

            for(let i = 0; i < LINE_NUMBER; i++) {
                //
                brickArray[i] = new Array(BRICK_PER_LINE);

                //
                for(let j = 0; j < BRICK_PER_LINE; j++) {
                    //
                    brickArray[i][j] = 1;
                }
            }
            
            return 1;
        }

        //
        function ballDisplay(ctx, x, y) {
            ctx.fillStyle = BALL_COLOR;
            ctx.beginPath();
            ctx.arc(x, y, BALL_RADIUS, 0, Math.PI*2, true);
            ctx.closePath();
            ctx.fill();
        }

        //
        function bricksDisplay(ctx) {
            let hasWon = true;
            
            for(let i = 0; i < brickArray.length; i++) {
                ctx.fillStyle = BRICK_COLORS[i];
                
                //
                for(let j = 0; j < brickArray[i].length; j++) {
                    if(brickArray[i][j] === 1) {
                        ctx.fillRect(
                            j * (BRICK_WIDTH + BRICK_MARGIN),
                            i * (BRICK_HEIGHT + BRICK_MARGIN),
                            BRICK_WIDTH,BRICK_HEIGHT);
                        hasWon = false; //
                    }
                }
            }
            return hasWon;
        }

        function racketDisplay(ctx) {
            ctx.fillStyle = "#333333";
            ctx.fillRect(racketX, racketY, RACKET_WIDTH, RACKET_HEIGHT);
        }

        function getNextPosition(position, direction)
        {
            return position + direction * BALL_SPEED;
        }

        function positionOutOfLimit(position, direction, minLimit, maxLimit)
        {
            let nextPosition = getNextPosition(position, direction);

            if(miLimit >= 0 && nextPosition < minLimit) {
                return true;
            }

            if(maxLimit >= 0 && nextPosition > maxLimit) {
                return true;
            }
            return false;
        }


        // 
        function computeDirections() {
            //
            if(positionOutOfLimit(ballX, dirBallX, 0, gameWidth)) {
                dirBallX = dirBallX * -1;
            }

            if(!positionOutOfLimit(ballX, dirBallX, racketX, racketX + RACKET_WIDTH) &&
                positionOutOfLimit(ballY, dirBallY, -1, gameHeight - RACKET_HEIGHT)) {
                dirBallY = -1;
                dirBallX = 2 * (ballX - (racketX + RACKET_WIDTH / 2)) / RACKET_WIDTH;
            } else if(positionOutOfLimit(ballY, dirBallY, -1, gameHeight)) {
                gameOver();
                return false;
            } else if(positionOutOfLimit(ballY, dirBallY, 0, -1)) {
                dirBallY = 1;
            }

            return true;
        }

        //
        function brickCollisionTest() {
            if(ballY <= (BRICK_MARGIN + BRICK_HEIGHT) * LINE_NUMBER) {
                //
                let positionY = Math.floor(ballY / (BRICK_HEIGHT + BRICK_MARGIN));
                let positionX = Math.floor(ballX / (BRICK_WIDTH + BRICK_MARGIN));

                if(brickArray[positionY][positionX] === 1) {
                    brickArray[positionY][positionX] = 0;
                    dirBallY = 1;
                }
            }
        }

        function gameOver() {
            clearInterval(session);
            alert("Game over !");
        }

        function winner() {
            clearInterval(session);
            alert("You won !");
        }

        //
        function refreshGame() {
            context = getContext();

            //
            clearContext(context, 0, gameWidth, 0, gameHeight);

            //
            if(bricksDisplay(context)) {
                winner();
                return;
            }

            raspberryDisplay(context);

            //
            if(!computeDirections()) {
                return;
            }

            brickCollisionTest();

            //
            ballX += dirBallX * BALL_SPEED;
            ballY += dirBallY * BALL_SPEED;

            ballDisplay(context, ballX, ballY);
        }

        //
        function moveCheck(e) {
            //
            if(e.keyCode === 39) {
                if((racketX + MOVE_STEP + RACKET_WIDTH) <= gameWidth) {
                    racketX += MOVE_STEP;
                }
            }
            //
            else if(e.keyCode === 37) {
                if((racketX - MOVE_STEP) >= 0) {
                    racketX -= MOVE_STEP;   
                }
            }
        }

        //
        window.addEventListener('load', function () {
            //
            context = getContext();

            //
            gameInit();
            racketInit();
            bricksInit(context);

            //
            bricksDisplay(context);

            //
            session = setInterval(refreshGame, 10);

            //
            window.document.onkeydown = moveCheck;

        }, false);
